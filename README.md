autofit
=======

A refactored version of the public domain 'autofit' module for the [Monkey programming language](https://github.com/blitz-research/monkey).

The original module was developed by James "DruggedBunny" Boyd. Due to a lack of a proper license, I can only assume the statements of "[Public domain code]" allows me the right to sublicense. That being said, this code-base contains very little of the original source, and is only identical name-wise for the sake of compatibility with existing applications.

**Features:**
* Compatibility with the original 'autofit' module.
* Internal 'Strict' language conformity.
* Sub-displays: Sub-displays can be used to have a "picture-in-picture" or "split-screen" effect in games.
* Support for matrix operations within Mojo. (Scaling and position related)
* Custom border colors, and border-draw toggling.
* Easy integration of sub-displays and sub-displays for those displays. (Through simple property overloading; see 'CameraDisplay')

**References:**
* [The original forum thread for 'autofit'](http://www.monkey-x.com/Community/posts.php?topic=1500&page=1).
